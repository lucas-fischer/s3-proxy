FROM node:18-alpine
MAINTAINER Lucas Fischer <lucas.fischer@me.com>

WORKDIR /srv

COPY package.json package-lock.json ./

RUN npm ci

COPY . .

HEALTHCHECK CMD ["wget", "-qO-", "http://localhost:8000/health"]

CMD NODE_ENV=production node index.js
