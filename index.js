require('dotenv').config()

const Url = require('url')
const micro = require('micro')
const microCors = require('micro-cors')
const mime = require('mime')
const s3 = require('./s3')

const { createError, send } = micro
const BUCKET = process.env.S3_BUCKET
const ONE_YEAR = 60 * 60 * 24 * 365
const PORT = process.env.PORT || 8000
const IS_CORS_ENABLED = process.env.IS_CORS_ENABLED
const CORS_DOMAIN = process.env.CORS_DOMAIN

const cors = microCors({ origin: CORS_DOMAIN })

const handler = async (req, res) => {
	const { pathname } = Url.parse(req.url)

	// Healthcheck
	if (pathname === '/health') {
		return send(res, 200)
	}

	// Get S3 Key
	const Key = decodeURI(pathname.substr(1))
	if (Key.length === 0) {
		throw createError(404, `Not Found: ${pathname}`)
	}

	// Load File frrom S3 and serve it
	try {
		const { ContentType, ContentLength, LastModified } = await s3
			.headObject({ Bucket: BUCKET, Key })
			.promise()

		// Lookup content type when not defined by S3
		const outputContentType =
			ContentType === 'application/octet-stream'
				? mime.getType(Key)
				: ContentType
		res.setHeader('Content-Type', outputContentType) // Set Correct Content-Type
		res.setHeader('Cache-Control', `public, max-age=${ONE_YEAR}, immutable`) // Cache for one Year

		// Handle Range Request correctly
		const range = req.headers.range || ''
		if (range) {
			console.log(`${pathname} 206 ${ContentLength}b`)

			// Calculate headers
			const total = ContentLength
			const bytes = range.replace(/bytes=/, '').split('-')
			const start = parseInt(bytes[0], 10)
			const end = bytes[1] ? parseInt(bytes[1], 10) : total - 1
			const chunksize = end - start + 1

			res.setHeader('Accept-Ranges', 'bytes')
			res.setHeader('Content-Length', chunksize)
			res.setHeader('Content-Range', 'bytes ' + start + '-' + end + '/' + total)
			res.setHeader('Last-Modified', LastModified)

			const params = { Bucket: BUCKET, Key, Range: range }
			return send(
				res,
				206, // Status-Code
				s3.getObject(params).createReadStream()
			)
		} else {
			// Default Request
			res.setHeader('Accept-Ranges', 'bytes')
			res.setHeader('Content-Length', ContentLength)

			console.log(`${pathname} 200 ${ContentLength}b`)
			return send(
				res,
				200, // Status-Code
				s3.getObject({ Bucket: BUCKET, Key }).createReadStream()
			)
		}
	} catch (err) {
		if (err.statusCode === 404) {
			throw createError(404, `Not Found: ${pathname}`)
		} else throw err
	}
}

const server = IS_CORS_ENABLED ? micro(cors(handler)) : micro(handler)

server.listen(PORT)

console.log(`Server is listening on PORT: ${PORT}`)
