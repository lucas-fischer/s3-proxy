# S3 Proxy

> Proxies files from S3-compatible storages via HTTP for better client and proxy caching.

## Configuration

The HTTP-server listens on port 8000. Variables marked with \* are required.

| Var                       | Description                                     |
| ------------------------- | ----------------------------------------------- |
| `S3_BUCKET` \*            | name of bucket                                  |
| `S3_ACCESS_KEY_ID` \*     | your S3 key id                                  |
| `S3_SECRET_ACCESS_KEY` \* | your S3 access key                              |
| `S3_ENDPOINT`             | changes the endpoint from aws to a custom one   |
| `PORT`                    | default: 8000                                   |
| `IS_CORS_ENABLED`         | default: false, set to true to enable CORS      |
| `CORS_DOMAIN`             | changes allowed origin from `*` to a custom one |
