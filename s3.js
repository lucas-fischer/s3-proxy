const S3 = require('aws-sdk/clients/s3')
const AWS = require('aws-sdk/global')

const { S3_ENDPOINT, S3_ACCESS_KEY_ID, S3_SECRET_ACCESS_KEY } = process.env

const endpoint = S3_ENDPOINT ? new AWS.Endpoint(S3_ENDPOINT) : undefined

// Configuring the AWS environment
AWS.config.update({
	accessKeyId: S3_ACCESS_KEY_ID,
	secretAccessKey: S3_SECRET_ACCESS_KEY
})

module.exports = new S3({ endpoint })
